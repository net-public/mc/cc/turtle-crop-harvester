-- CC:Tweaked / ComputerCraft
-- Farm Harvester Turtle (2021-12-31)
-- version 0.0.6.alpha
-- NetVip3r

local isDebug = true
local isCraftOS
if periphemu then
    isCraftOS = true
end

--------------------
-- Variables
--------------------
local config = {
    sleepTime   = 120,      -- How many secs to wait between runs
    length      = 0,        -- Max distance to travel
    --width       = 0,        -- Max distance to sides, not implemented
    timesSuck   = 4,        -- How many times to 'suck' ground items
    
    fuelSlot    = 1,        -- Turtle Slot used for fuel
    seedSlot    = 2,        -- Turtle Slot used for seeds

    script      = { 
        name = "Turtle Farming Script",
        version     = {
            group = "2021-12-31",
            major = 0,
            minor = 6,
            status = "alpha",
        },
    },
}

local system = {
    relativePos = {             -- Relative Pos from Start
        x = 0,
        y = 0,
        z = 0,
    },
    
    distance    = 0,            -- Distance traveled
    
    config      = false,        -- Whether the system has been config'd
    fuel        = 0,
    
    quit        = false,
    errorCode   = nil,
    errorMsg    = nil,
    errorFunc   = nil,
}


------------------------------
-- Functions / Subroutines
------------------------------
function parseError(code, msg, _func)
    --[[
    0 - Ended fine
    1 - No 'combustible fuel' in Slot 1
    2 - No 'replacement seed' in Slot 2
    3 - Environment Error
    ]]--
    if (msg == nil) then msg = "" end
    
    local _msg
    if (turtle) or (isCraftOS) then
        if (code == 1) then
            _msg = "Error: Inventory Slot 1\n"
            _msg = _msg .. msg
        elseif (code == 2) then
            _msg = "Error: Inventory Slot 2\n"
            _msg = _msg .. msg
        elseif (code == 3) then
            _msg = "Error: Environmental\n"
            _msg = _msg .. msg
        else
            --_msg = "Terminated OK"
        end
    end
    
    system.errorCode    = nil
    system.errorMsg     = nil
    system.errorFunc    = nil
    
    --term.clear()
    --term.setCursorPos(1, 1)
    if (_msg) then
        term.setCursorPos(1, 4)
        print(_msg)
    end
    
    if (isDebug) and (_func) then
        print(_func)
    end
end

function quit()
    system.quit = true
end

function setError(code, msg, func)
    system.errorCode    = code
    system.errorMsg     = msg
    system.errorFunc    = func
    quit()
end

function parseCommandline()
    --[[
        script      = { 
        name = "Turtle Farming Script",
        version     = {
            group = "2021-12-15",
            major = 0,
            minor = 4,
            status = "alpha",
        },
        --]]

    if arg[1] then
        if arg[1] == "--help" then
            -- print help to screen
            --print("help information goes here.")
            --print("If this msg is still here, I may have forgotten it")
            --print("This script allows a 'Farming Turtle' to harvest crops in a straight line and returns to deposit it's harvest.")
            print("[Setup]\nPlace a chest on the block adjacent to the 'farmland' block. Above the chest, a 'Farming Turtle' facing the direction of travel. In the turtle's inventory 'slot 1' place a combustible fuel. In 'slot 2', a few of the target's seeds. Then start the script.\n")
            print("[Script]\n  " .. arg[0] .. " <number>\n<number> = Number of blocks to harvest")
            quit()
            
        elseif arg[1] == "--version" then
            -- print version information
            local _v    = config.script.version
            local _ver  = _v.group .. "." .. tostring(_v.major) .. "." ..
                tostring(_v.minor) .. "." .. _v.status
            print(arg[0] .. " (" .. _ver .. ")")
            print(config.script.name)
            print("\nWritten by NetVip3r")
            quit()
        else
            -- parse 'length' from command line
            config.length = tonumber(arg[1])
            print("Harvest length set to " .. arg[1] .. " blocks")
            --quit()
        end
    else
        print("Missing Length (\"" .. arg[0] .. " --help\" for help)\n")
        print("Usage:\n")
        print("  " .. arg[0] .. " --help")
        print("  " .. arg[0] .. " --version")
        print("  " .. arg[0] .. " <number>")
        quit()
    end
    
end

function handleRefuel()
        local _fCurrent     = turtle.getFuelLevel()
        local _fSlot        = config.fuelSlot
        local _fGetCount    = turtle.getItemCount(_fSlot)
        local _fExpectUse   = config.length * 2
        
        turtle.select(_fSlot)
        
        local _fSuccess, _fReason = turtle.refuel(0)
        
        if (_fSuccess) then
            if (_fCurrent < _fExpectUse) then
                -- Fuel not enough to go and come back, refuel
                if (_fGetCount > _fExpectUse) then
                    _fSuccess, _fReason = turtle.refuel()
                else
                    --_fError = true
                    --setError(1, "Not enough fuel to cover the distance.")
                    _fSuccess = false
                    _fReason = "Not enough fuel to cover the distance."
                end
            end
        end
        
        return _fSuccess, _fReason
end

function handleFuel()
    local _fSuccess, _fReason
    
    if (turtle) then
        local _fCurrent     = turtle.getFuelLevel()
        local _fLimit       = turtle.getFuelLimit()
        local _fSlot        = config.fuelSlot
        local _fGetCount    = turtle.getItemCount(_fSlot)
        
        if (_fCurrent == "unlimited") then
            -- "Unlimited" Fueled Turtle
            system.fuel = 100
            _fSuccess = true
            
        else
            -- "Normal" Fueled Turtle
            
            -- If at start position, handle refuel
            if (system.distance == 0) then
                _fSuccess, _fReason = handleRefuel()
            end
            
            -- Update Fuel Gauge percentage
            system.fuel = math.ceil((_fCurrent / _fLimit) * 100)
        end
    else
        _fSuccess = false
        _fReason = "This isn't a turtle"
    end
    
    return _fSuccess, _fReason
end

function handleSuck(_times)
    _times = _times or 1
    
    for _i = 1, _times do
        turtle.suckDown()
    end
end

function handleOffloadToChest()
    --local prev_slot = turtle.getSelectedSlot()
    local _fSuccess, _chest = turtle.inspectDown()
    
    if (_fSuccess) then
        if _chest.tags["forge:chests"] then
            for i = 3, 16 do
                local ok = turtle.select(i)
                if ok then turtle.dropDown() end
            end
        end
    end
    --turtle.select(prev_slot)
end

function handleReturn()
    --print("reached the end")
    --quit()
    turtle.turnRight()
    turtle.turnRight()

    for i = 1, system.distance do
        turtle.forward()
        if i > 1 then turtle.suckDown() end
    end
    system.distance = 0

    -- Unload into chest in front
    handleOffloadToChest()
    --getFuelFromChest()

    turtle.turnRight()
    turtle.turnRight()

    --os.sleep(config.sleepTime)
end

function handleStep()
        -- Handle the fuel situation first
        --handleFuel()

        local _sSlot        = config.seedSlot
        local _sGetCount    = turtle.getItemCount(_sSlot)

        if (system.distance == config.length) then
            -- at max 'length', return mode
            handleReturn()
            os.sleep(config.sleepTime)
        else
            -- move and work
            if (_sGetCount > 0) then
                local _tMove = turtle.forward()
                if (_tMove == true) then
                    -- Moved forward ok
                    system.distance = system.distance + 1
                    turtle.select(_sSlot)
                    if (turtle.compareDown()) then
                        -- Seed in slot matches crop underneath
                        -- Check state of crop
                        local inspect, data = turtle.inspectDown()
                        if inspect then
                            if data.state.age == 7 then
                                local dig, reason = turtle.digDown()
                                if dig then
                                    -- Turtle harvested OK
                                    handleSuck(config.timesSuck)
                                    turtle.placeDown()
                                end
                            end
                        end
                    end
                else
                    -- error, blocked
                    system.errorCode = 3
                    system.errorMsg = "Can't Move: " .. reason
                    system.errorMsg = "handleStep()"
                end
            else
                -- No Seed in slot2
                if (system.distance ~= 0) then
                    handleReturn()
                    setError(2, "No seeds found, returning", "handleStep()")
                else
                    setError(2, "No seeds found", "handleStep()")
                end
            end
        end
end

function drawTerminal()
    local _cID      = os.getComputerID()
    local _cLabel   = os.getComputerLabel() or "Turtle"
    local _cFuel    = system.fuel

    term.clear()
    
    term.setCursorPos(1, 1)
    term.write("---------------------------------------")
    term.setCursorPos(1, 3)
    term.write("---------------------------------------")
    term.setCursorPos(1, 13)
    term.write("---------------------------------------")
    
    term.setCursorPos(2, 1)
    term.write("[Harvesting - " ..
        tostring(config.length) .. " Blocks]")
    
    term.setCursorPos(1, 2)
    term.write("/ #" .. _cID .. " " .. _cLabel )
    term.setCursorPos(29, 2)
    term.write("Fuel: " .. _cFuel .. "%")
    
    term.setCursorPos(1, 4)
end


------------------------------
-- Functions - Main Loop
------------------------------
function _init()
    local _s = config.script
    local _ver = 
        _s.version.group .. "." .. tostring(_s.version.major) .. "." ..
        tostring(_s.version.minor) .. "." .. _s.version.status
            
    term.clear()
    term.setCursorPos(1, 1)
    
    -- parse commandline args / check slots
    parseCommandline()
        
    --sleep(1)
    --quit()
    
end

function _update()
    local _fOK      = true
    local _fReason
    
    if (system.distance == 0) then
        -- handle fuel level/refueling
        _fOK, _fReason = handleFuel()
        if (not _fOK) then
            setError(1, _fReason, "_update()")
        end
    end
    
    
    if (_fOK) then
        -- next working step
        handleStep()
    end
    
end

function _draw()
    drawTerminal()
end

------------------------------
-- Main Loop
------------------------------
-- initialize script
if _init then _init() end

-- loop
while(not system.quit) do
    if _update then _update() end
    if _draw then _draw() end

    --if (system.quit) then break end

end

parseError(system.errorCode, system.errorMsg, system.errorFunc)
--if system.errorCode then parseError(system.errorCode) end
--print("Script Terminated")
